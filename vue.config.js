module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],
  chainWebpack: config => {
    config.module
        .rule('graphql')
        .test(/\.graphql$/)
        .use('graphql-tag/loader')
        .loader('graphql-tag/loader')
        .end();
  },
  publicPath: process.env.NODE_ENV === 'production'
      ? '/information-organisation/'
      : '/'
}